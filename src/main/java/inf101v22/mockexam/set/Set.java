package inf101v22.mockexam.set;

import java.util.ArrayList;
import java.util.Iterator;

public class Set implements ISet {

    //create new arraylist
    ArrayList<Object> list = new ArrayList<Object>();

    @Override
    public Iterator iterator() {
        // return iterator over list
        return list.iterator();
    }

    @Override
    public int size() {
        // return size of list
        return list.size();
    }

    @Override
    public void add(Object element) {
        // add element to the set if it is not already in the set
        if (!list.contains(element)) {
            list.add(element);
        }
    }

    @Override
    public void addAll(Iterable other) {
        // add all elements from other to the set
        for (Object element : other) {
            add(element);
        }
    }

    @Override
    public void remove(Object element) {
        //remove element from set
        list.remove(element);
    }

    @Override
    public boolean contains(Object element) {
        // return true if list contains element
        return list.contains(element);
    }

    @Override
    public ISet union(ISet other) {
        //create union of set and other
        Set union = new Set();
        union.addAll(this);
        union.addAll(other);
        return union;
    }

    @Override
    public ISet intersection(ISet other) {
        //create intersection
        Set intersection = new Set();
        for (Object element : this) {
            if (other.contains(element)) {
                intersection.add(element);
            }
        }
        return intersection;
    }

    @Override
    public ISet complement(ISet other) {
        //create complement
        Set complement = new Set();
        for (Object element : this) {
            if (!other.contains(element)) {
                complement.add(element);
            }
        }
        return complement;
    
    }

    @Override
    public ISet copy() {
        //create copy
        Set copy = new Set();
        copy.addAll(this);
        return copy;
    }

    
}
