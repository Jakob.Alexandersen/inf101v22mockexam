package inf101v22.mockexam.traffic.control;

import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;
import inf101v22.mockexam.traffic.model.TrafficLightControllable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

public class TrafficLightController implements ITrafficGuiController {

    private TrafficLightControllable model;
    private Timer timer;

    public TrafficLightController(TrafficLightControllable model) {
        this.model = model;
        ActionListener listener = this::timerFired;
        timer = new Timer(model.minMillisInCurrentState(), listener);
        timer.start();
    }

    private void timerFired(ActionEvent e) {
        model.goToNextState();
        timer.setInitialDelay(model.minMillisInCurrentState());
        timer.restart();
    }

    @Override
    public Observable<Boolean> isPaused() {
        return new ControlledObservable<Boolean>(false);
    }

    @Override
    public void pausePressed(ActionEvent e) {
        //pause timer 
        timer.stop();
    }

    @Override
    public void startPressed(ActionEvent e) {
        timer.start();
    }
}





