package inf101v22.mockexam.traffic.model;

import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;

public class TrafficLightModel implements TrafficLightControllable{

    public ControlledObservable<Boolean> redIsOn = new ControlledObservable<Boolean>(true);
    public ControlledObservable<Boolean> yellowIsOn = new ControlledObservable<Boolean>(false);
    public ControlledObservable<Boolean> greenIsOn = new ControlledObservable<Boolean>(false);

    @Override
    public Observable<Boolean> redIsOn() {
        //call on observable to check if red is on
        return redIsOn;

    }

    @Override
    public Observable<Boolean> yellowIsOn() {
        //call on observable to check if yellow is on
        return yellowIsOn;
    }

    @Override
    public Observable<Boolean> greenIsOn() {
        //call on observable to check if green is on
        return greenIsOn;
    }

    @Override
    public void goToNextState() {
        //go to the next state of the traffic light
        if (redIsOn.getValue() == true && yellowIsOn.getValue() == false && greenIsOn.getValue() == false) {
            yellowIsOn.setValue(true);
        } else if (redIsOn.getValue() == true && yellowIsOn.getValue() == true && greenIsOn.getValue() == false) {
            redIsOn.setValue(false);
            yellowIsOn.setValue(false);
            greenIsOn.setValue(true);
        } else if (greenIsOn.getValue() == true && yellowIsOn.getValue() == false && redIsOn.getValue() == false) {
            yellowIsOn.setValue(true);
            greenIsOn.setValue(false);
        } else if (redIsOn.getValue() == false && yellowIsOn.getValue() == true && greenIsOn.getValue() == false){
            redIsOn.setValue(true);
            yellowIsOn.setValue(false);
        }
    }

    @Override
    public int minMillisInCurrentState() {
        //return value based on the current state of the traffic light
        if (redIsOn.getValue() == true && yellowIsOn.getValue() == false && greenIsOn.getValue() == false) {
            return 2000;
        } else if (redIsOn.getValue() == true && yellowIsOn.getValue() == true && greenIsOn.getValue() == false) {
            return 500;
        } else if (greenIsOn.getValue() == true && yellowIsOn.getValue() == false && redIsOn.getValue() == false) {
            return 2000;
        } else if (redIsOn.getValue() == false && yellowIsOn.getValue() == true && greenIsOn.getValue() == false){
            return 1000;
        }
        else {
            return 0;
        }
    }
    
}
