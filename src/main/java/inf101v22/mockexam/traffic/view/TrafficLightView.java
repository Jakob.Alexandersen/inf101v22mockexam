package inf101v22.mockexam.traffic.view;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JComponent;

import inf101v22.mockexam.traffic.model.TrafficLightViewable;

public class TrafficLightView extends JComponent {
    
    //constructor
    public TrafficLightView(TrafficLightViewable model) {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(new LampView(model.redIsOn(), Color.RED));
        this.add(new LampView(model.yellowIsOn(), Color.YELLOW));
        this.add(new LampView(model.greenIsOn(), Color.GREEN));
    }

    //create lampview for red lamp


}
