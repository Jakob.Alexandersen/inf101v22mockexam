package inf101v22.mockexam.traffic.view;

import inf101v22.mockexam.observable.Observable;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;
import javax.swing.JComponent;


public class LampView extends JComponent{

    Observable<Boolean> isOn;
    Color color;

    //constructor 
    public LampView(Observable<Boolean> isOn, Color color) {
        // add this::repaint as observer to isOn
        this.isOn = isOn;
        this.color = color;
        isOn.addObserver(this::repaint);
    }

    @Override
    public void paintComponent(Graphics g) {
        //paint the lamp
        super.paintComponent(g);
        if (isOn.getValue() == false) {
            g.setColor(color.darker().darker());
            g.fillOval(0, 0, getWidth(), getHeight());
        } else {
            g.setColor(color);
            g.fillOval(0, 0, getWidth(), getHeight());
        }
    }

    @Override
    public Dimension getPreferredSize() {
        //return the preferred size of the lamp
        return new Dimension(50, 50);
    }
    
}
